#include <vector>
#include <string>
#include <utility>

enum class content_t {unknown, empty, tree};

class cell_t {
    int m_region_value;
    content_t m_content;
    int m_hypothesis_level = 0; // level 0 is where we know the value, others are hypothetical
    bool is_obvious_cell;
public:
    cell_t(int rv, content_t c = content_t::unknown)
        : m_region_value{rv}
        , m_content{c}
        , is_obvious_cell{false}
    {}

    int region_value() const {return m_region_value;}
    content_t content() const {return m_content;}
    void set_content(content_t v, int level = 0) {m_content = v; m_hypothesis_level = level; is_obvious_cell=false;}
    void set_obvious_content(content_t v, int level = 0) {m_content = v; m_hypothesis_level = level; is_obvious_cell=true;}
    int hypothesis_level() const { return m_hypothesis_level; }
    bool is_obvious() const {return is_obvious_cell;}
};

bool operator==(cell_t const& a, cell_t const& b);

using schema_t = std::vector<std::vector<cell_t> >;

class treesolver {
    schema_t schema;
    const int Trees_x_Region;
    const int N;
    const int Max_Region;
    int current_hypothesis_level;
    bool m_silent;
    bool m_obvious;
    bool m_super;
    bool m_verbose;;
    mutable int count_hypothesis;
    mutable int count_trees;
    mutable int count_set;
public:
    treesolver(schema_t const& scm, int Trees_x_Region, bool silent=false, bool verbose=false, bool fill_obvious=true, bool be_super=false);
    schema_t solve();
    schema_t brute_solve();
private:
    schema_t clear_schema() const;
    void fill_up_eventual_presets();
    void be_super_smart();
    void fill_obvious_cells();
    void fill_around_tree(int, int);
    int max_region_im_schema();
    static std::string print_n(char c, int n);
    void print_schema() const;
    int find_most_constrained_region();
    bool is_valid_so_far() const;
    std::pair<int, int> find_target_for_region(int region) const;
    std::pair<int, int> find_first_availble_cell() const;
    bool is_valid() const;
    void create_tree_hypothesis(int i, int j);
    void cancel_hypothesis();
};
