#include <iostream>
#include <utility>
#include <cmath>
#include <string>
#include <iomanip>
#include <cassert>
#include <chrono>
#include "treesolver.hpp"

bool operator==(cell_t const& a, cell_t const& b) {
    return a.region_value() == b.region_value() and a.content() == b.content();
}


treesolver::treesolver(schema_t const& scm, int Trees_x_Region, bool silent, bool verbose, bool obvious, bool be_super)
    : schema{scm}
    , Trees_x_Region{Trees_x_Region}
    , N{(int)schema.size()}
    , Max_Region{max_region_im_schema()}
    , current_hypothesis_level{0}
    , m_silent{silent}
    , m_obvious{obvious}
    , m_super{be_super}
    , m_verbose{!silent?verbose:false}
    , count_hypothesis{0}
    , count_trees{0}
    , count_set{0}
    {
        fill_up_eventual_presets();
        is_valid_so_far();
        if (m_obvious) fill_obvious_cells();
        if (!m_silent) print_schema();
        if (!m_silent) std::cout << "\n";
    }

void treesolver::fill_around_tree(int i, int j) {
    for (int l=-1; l<2; ++l) {
        for (int m=-1; m<2; ++m) {
            if (l!=0 or m!=0) {
                if (i+l>=0 and j+m>=0 and i+l<N and j+m<N) {

                    assert(schema[i+l][j+m].content() != content_t::tree);

                    if (schema[i+l][j+m].content() == content_t::unknown) {
                        schema[i+l][j+m].set_content(content_t::empty, current_hypothesis_level);
                        count_set++;
                    }
                }
            }
        }
    }
}

void treesolver::fill_up_eventual_presets() {
    for (int i=0; i<N; ++i) {
        for (int j=0; j<N; ++j) {
            if (schema[i][j].content() == content_t::tree) {
                fill_around_tree(i,j);
            }
        }
    }
}

int treesolver::max_region_im_schema() {
    int res = 0;
    for (int i=0; i<N; ++i) {
        for (int j=0; j<N; ++j) {
            if (schema[i][j].region_value() > res) {
                res = schema[i][j].region_value();
            }
        }
    }
    return res;
}

std::string treesolver::print_n(char c, int n) {
    std::string s;
    for (int i=0; i<n; ++i)
        s = s + c;
    return s;
}

void treesolver::print_schema() const {

    int n_digit = (int)std::log10(Max_Region+1)+1;
    int cell_width = 1 + 1 + 1 + 1 +n_digit + 1 +1;

    for (int i=0; i<N; ++i) {
        for (int j=0; j<N; ++j) {
            std::cout << "|" << print_n('-', cell_width) << "|";
        }
        std::cout << "\n";

        for (int j=0; j<N; ++j) {
            std::string c = schema[i][j].content()==content_t::tree?"\033[1;32mT\033[0m":(schema[i][j].content()==content_t::empty?"\033[1;31m*\033[0m":" ");
            std::cout << "| " << c << " <" << std::setw(n_digit) << schema[i][j].region_value() << "> |";
        }
        std::cout << "\n";
    }
    for (int j=0; j<N; ++j) {
        std::cout << "|" << print_n('-', cell_width) << "|";
    }
    std::cout << "\n";
}

int treesolver::find_most_constrained_region() {
    std::vector<int> count(Max_Region+1);

    for (int region=0; region <= Max_Region; ++region) {
        for (int i=0; i<N; ++i) {
            for (int j=0; j<N; ++j) {
                if (schema[i][j].region_value() == region) {
                    if (schema[i][j].content() == content_t::unknown) {
                        count[region]++;
                    }
                }
            }
        }
    }
    int min=N*N;
    int r = -1;
    for (int i=0; i<=Max_Region; ++i) {
        //std::cout << "region " << i << " has size " << count[i] << "\n";
        if (count[i] != 0 and count[i] < min) {
            min = count[i];
            r = i;
        }
    }
    return r;
}

std::pair<int, int> treesolver::find_target_for_region(int region) const {
    // find the last unknown cell
    int ri=-1;
    int rj=-1;
    for (int i=0; i<N; ++i) {
        for (int j=0; j<N; ++j) {
            if (schema[i][j].region_value() == region) {
                if (schema[i][j].content() == content_t::unknown) {
                    ri=i;
                    rj=j;
                }
            }
        }
    }

    assert(ri != -1 and rj != -1);

    return {ri,rj};
}


bool treesolver::is_valid_so_far() const {
    bool res = true;

    // check columns
    for (int i=0; i<N; ++i) {
        int trees = 0;
        int unknown = 0;
        for (int j=0; j<N; ++j) {
            if (schema[i][j].content() == content_t::tree) {
                trees++;
            }
            if (schema[i][j].content() == content_t::unknown) {
                unknown++;
            }
        }
        if (trees > Trees_x_Region or trees+unknown < Trees_x_Region) {
            res=false;
            //std::cout << "columns " << i << std::endl;
        }
    }
    for (int j=0; j<N; ++j) {
        int trees = 0;
        int unknown = 0;
        for (int i=0; i<N; ++i) {
            if (schema[i][j].content() == content_t::tree) {
                trees++;
            }
            if (schema[i][j].content() == content_t::unknown) {
                unknown++;
            }
        }
        if (trees > Trees_x_Region or trees+unknown < Trees_x_Region) {
            res=false;
            //std::cout << "rows " << j << std::endl;
        }
    }
    for (int region=0; region<=Max_Region; region++) {
        int trees = 0;
        int unknown = 0;
        for (int j=0; j<N; ++j) {
            for (int i=0; i<N; ++i) {
                if (schema[i][j].region_value() == region) {
                    if (schema[i][j].content() == content_t::tree) {
                        trees++;
                    }
                    if (schema[i][j].content() == content_t::unknown) {
                        unknown++;
                    }
                }
            }
        }
        if (trees > Trees_x_Region or trees+unknown < Trees_x_Region) {
            res=false;
            //std::cout << "region " << region << std::endl;
        }
    }
    return res;
}

bool treesolver::is_valid() const {
    bool res = true;

    // check columns
    for (int i=0; i<N; ++i) {
        int trees = 0;
        for (int j=0; j<N; ++j) {
            if (schema[i][j].content() == content_t::tree) {
                trees++;
            }
        }
        if (trees != Trees_x_Region) {
            res=false;
            //std::cout << "columns " << i << std::endl;
        }
    }
    for (int j=0; j<N; ++j) {
        int trees = 0;
        for (int i=0; i<N; ++i) {
            if (schema[i][j].content() == content_t::tree) {
                trees++;
            }
        }
        if (trees != Trees_x_Region) {
            res=false;
            //std::cout << "rows " << j << std::endl;
        }
    }
    for (int region=0; region<=Max_Region; region++) {
        int trees = 0;
        for (int j=0; j<N; ++j) {
            for (int i=0; i<N; ++i) {
                if (schema[i][j].region_value() == region) {
                    if (schema[i][j].content() == content_t::tree) {
                        trees++;
                    }
                }
            }
        }
        if (trees != Trees_x_Region) {
            res=false;
            //std::cout << "region " << region << std::endl;
        }
    }
    return res;
}

void treesolver::create_tree_hypothesis(int i, int j) {

    assert(schema[i][j].content() == content_t::unknown);

    current_hypothesis_level++;

    schema[i][j].set_content(content_t::tree, current_hypothesis_level);
    count_set++;
    count_trees++;

    // Mark the points around the cell
    fill_around_tree(i,j);
    //print_schema();

    if (m_obvious) fill_obvious_cells();
}

void treesolver::be_super_smart() {
    int stats=0;

    if (Trees_x_Region != 2) return;

    for (int r=0; r<=Max_Region; ++r) {
        // std::cout << "\nregion " << r << "\n\n";
        int rect_begin_x = N;
        int rect_begin_y = N;
        int rect_end_x = 0;
        int rect_end_y = 0;
        int ntrees = 0;

        //find the bounding box of the unknown cells in the region. If
        // the region is 3x2 of 1x3, or 3x3 do something special.
        // Certain levels seem to benefit a lot from this, let's see
        // if it true!
        for (int i=0; i<N; ++i) {
            for (int j=0; j<N; ++j) {
                if (schema[i][j].region_value() == r and schema[i][j].content() == content_t::tree) {
                    ntrees++;
                }
                if (schema[i][j].region_value() == r and schema[i][j].content() == content_t::unknown) {
                    if (i < rect_begin_x) rect_begin_x = i;
                    if (j < rect_begin_y) rect_begin_y = j;
                    if (i > rect_end_x) rect_end_x = i;
                    if (j > rect_end_y) rect_end_y = j;
                }
            }
        }

        if (ntrees != 0) break;

        int shape_x = rect_end_x - rect_begin_x + 1;
        int shape_y = rect_end_y - rect_begin_y + 1;

        // std::cout << shape_x << " x " << shape_y
        //           << ", begin = (" << rect_begin_x << ", " << rect_begin_y << ")"
        //           << ", end = (" << rect_end_x << ", " << rect_end_y << ")"a
        //           << "\n";
        // print_schema();
        // std::cout << "\n";
        // I will not be very smart, just list the cases
        if (shape_x == 1 and shape_y == 3) {
            //std::cout << "got to be 1x3\n";
            if (schema[rect_begin_x][rect_begin_y+1].region_value() == r and schema[rect_begin_x][rect_begin_y+1].content() == content_t::unknown) {
                schema[rect_begin_x][rect_begin_y+1].set_content(content_t::empty, current_hypothesis_level);
                count_set++;
                stats++;
            }
        }

        if (shape_x == 3 and shape_y == 1) {
            //std::cout << "got to be 3x1\n";
            if (schema[rect_begin_x+1][rect_begin_y].region_value() == r and schema[rect_begin_x+1][rect_begin_y].content() == content_t::unknown) {
                schema[rect_begin_x+1][rect_begin_y].set_content(content_t::empty, current_hypothesis_level);
                count_set++;
                stats++;
            }
        }

        if (shape_x == 2 and shape_y == 3) {
            //std::cout << "got to be 2x3\n";
            if (schema[rect_begin_x][rect_begin_y+1].region_value() == r and schema[rect_begin_x][rect_begin_y+1].content() == content_t::unknown) {
                schema[rect_begin_x][rect_begin_y+1].set_content(content_t::empty, current_hypothesis_level);
                count_set++;
                stats++;
            }
            if (schema[rect_begin_x+1][rect_begin_y+1].region_value() == r and schema[rect_begin_x+1][rect_begin_y+1].content() == content_t::unknown) {
                schema[rect_begin_x+1][rect_begin_y+1].set_content(content_t::empty, current_hypothesis_level);
                count_set++;
                stats++;
            }
        }

        if (shape_x == 3 and shape_y == 2) {
            //std::cout << "got to be 3x2\n";
            if (schema[rect_begin_x+1][rect_begin_y].region_value() == r and schema[rect_begin_x+1][rect_begin_y].content() == content_t::unknown) {
                schema[rect_begin_x+1][rect_begin_y].set_content(content_t::empty, current_hypothesis_level);
                count_set++;
                stats++;
            }
            if (schema[rect_begin_x+1][rect_begin_y+1].region_value() == r and schema[rect_begin_x+1][rect_begin_y+1].content() == content_t::unknown) {
                schema[rect_begin_x+1][rect_begin_y+1].set_content(content_t::empty, current_hypothesis_level);
                count_set++;
                stats++;
            }
        }

        if (shape_x == 3 and shape_y == 3) {
            //std::cout << "got to be 3x3\n";
            if (schema[rect_begin_x+1][rect_begin_y+1].region_value() == r and schema[rect_begin_x+1][rect_begin_y+1].content() == content_t::unknown) {
                schema[rect_begin_x+1][rect_begin_y+1].set_content(content_t::empty, current_hypothesis_level);
                count_set++;
                stats++;
            }
        }
        //print_schema();
        //std::cout << "\n";
    } // for (int r=0; r<=Max_Region; ++r)
    if (m_verbose and stats>0) { std::cout << "(SUPER " << stats << ") ";};
}

void treesolver::fill_obvious_cells() {
    bool something_done = true;
    int n_it = 0;
    while (something_done) {
        something_done=false;
        for (int i=0; i<N; ++i) {
            for (int j=0; j<N; ++j) {
                int unknown_on_row = 0;
                int unknown_on_col = 0;
                int trees_on_row = 0;
                int trees_on_col = 0;

                for (int l=0; l<N; ++l) {
                    if ( schema[l][j].content() == content_t::tree) {
                        trees_on_row++;
                    }
                    if ( schema[i][l].content() == content_t::tree) {
                        trees_on_col++;
                    }
                    if ( schema[l][j].content() == content_t::unknown) {
                        unknown_on_row++;
                    }
                    if ( schema[i][l].content() == content_t::unknown) {
                        unknown_on_col++;
                    }
                }

                // Mark rows and columns
                if (Trees_x_Region == trees_on_row) {
                    for (int l=0; l<N; ++l) {
                        if (schema[l][j].content() == content_t::unknown) {
                            schema[l][j].set_content(content_t::empty, current_hypothesis_level);
                            something_done = true;
                            count_set++;
                        }
                    }
                }
                if (Trees_x_Region == trees_on_col) {
                    for (int l=0; l<N; ++l) {
                        if (schema[i][l].content() == content_t::unknown) {
                            schema[i][l].set_content(content_t::empty, current_hypothesis_level);
                            something_done = true;
                            count_set++;
                        }
                    }
                }

                if (trees_on_row < Trees_x_Region and unknown_on_row+trees_on_row == Trees_x_Region) {
                    for (int l=0; l<N; ++l) {
                        if (schema[l][j].content() == content_t::unknown) {
                            schema[l][j].set_obvious_content(content_t::tree, current_hypothesis_level);
                            something_done = true;
                            count_trees++;
                            count_set++;
                            fill_around_tree(l,j);
                        }
                    }
                }
                if (trees_on_col < Trees_x_Region and unknown_on_col+trees_on_col == Trees_x_Region) {
                    for (int l=0; l<N; ++l) {
                        if (schema[i][l].content() == content_t::unknown) {
                            schema[i][l].set_obvious_content(content_t::tree, current_hypothesis_level);
                            something_done = true;
                            count_trees++;
                            count_set++;
                            fill_around_tree(i,l);
                        }
                    }
                }
            }
        }
        for (int r=0; r<=Max_Region; ++r) {
            int unknown_on_reg = 0;
            int trees_on_reg = 0;

            for (int i=0; i<N; ++i) {
                for (int j=0; j<N; ++j) {
                    if (schema[i][j].region_value() == r) {
                        if (schema[i][j].content() == content_t::tree) {
                            trees_on_reg++;
                        }
                        if (schema[i][j].content() == content_t::unknown) {
                            unknown_on_reg++;
                        }
                    }
                }
            }

            if (Trees_x_Region == trees_on_reg) {
                for (int i=0; i<N; ++i) {
                    for (int j=0; j<N; ++j) {
                        if (schema[i][j].region_value() == r) {
                            if (schema[i][j].content() == content_t::unknown) {
                                schema[i][j].set_content(content_t::empty, current_hypothesis_level);
                                something_done = true;
                                count_set++;
                            }
                        }
                    }
                }
            }
            if (trees_on_reg < Trees_x_Region and unknown_on_reg+trees_on_reg == Trees_x_Region) {
                for (int i=0; i<N; ++i) {
                    for (int j=0; j<N; ++j) {
                        if (schema[i][j].region_value() == r) {
                            if (schema[i][j].content() == content_t::unknown) {
                                schema[i][j].set_obvious_content(content_t::tree, current_hypothesis_level);
                                count_trees++;
                                count_set++;
                                fill_around_tree(i,j);
                                something_done = true;
                            }
                        }
                    }
                }
            }
        }

        if (m_super) be_super_smart();
        n_it++;
    }
    if (m_verbose) { std::cout << "(OBVIOUS " << n_it << " iterations) ";};
    //print_schema();
}

void treesolver::cancel_hypothesis() {
    //std::cout << "CANCELING HYPOTHESIS " << current_hypothesis_level << "\n";
    for (int i=0; i<N; ++i) {
        for (int j=0; j<N; ++j) {
            if (schema[i][j].hypothesis_level() == current_hypothesis_level) {
                if (schema[i][j].content() == content_t::tree and not schema[i][j].is_obvious()) {
                    schema[i][j].set_content(content_t::empty, current_hypothesis_level-1);
                    count_set++;
                } else {
                    schema[i][j].set_content(content_t::unknown);
                    count_set++;
                }
            }
        }
    }
    current_hypothesis_level--;
    //print_schema();
}

schema_t treesolver::clear_schema() const {
    schema_t result{schema};
    for (int i=0; i<N; ++i) {
        for (int j=0; j<N; ++j) {
            if (result[i][j].content() == content_t::tree) {
                result[i][j].set_content(content_t::tree, 0);
                count_trees++;
                count_set++;
            } else {
                result[i][j].set_content(content_t::unknown, 0);
                count_set++;
            }
        }
    }
    return result;
}

schema_t treesolver::solve() {
    auto t_start = std::chrono::high_resolution_clock::now();

    int nsteps = 0;

    while (!is_valid()) {
        int region = find_most_constrained_region();
        //if (!m_silent) std::cout << "Terget region: " << region << "\n";
        auto p = find_target_for_region(region);
        //if (!m_silent) std::cout << "Selected cell (" << p.first << ", " << p.second << ")\n";
        create_tree_hypothesis(p.first, p.second);
        //if (!m_silent) std::cout << "valid so far " << std::boolalpha << is_valid_so_far() << std::endl;
        if (!is_valid_so_far()) {
            cancel_hypothesis();
            //if (!m_silent) std::cout << "(CANCELED) valid so far " << std::boolalpha << is_valid_so_far() << std::endl;
            while (not is_valid_so_far()) {
                cancel_hypothesis();
            }
        if (m_verbose) { std::cout << "(CANCELLED) "; }
        }
        //std::cout << std::boolalpha << "Valid "  << is_valid() << std::endl;
        //print_schema();
        if (m_verbose) { std::cout << "End of itaration: hyp. level: " << current_hypothesis_level << "\n"; print_schema(); }
        count_hypothesis++;;
    }

    auto t_end = std::chrono::high_resolution_clock::now();

    if (!m_silent) print_schema();
    std::cout << "brute=false"
              << " obvious=" << std::boolalpha << m_obvious
              << " smart=" << std::boolalpha << m_super
              << " (h t s) \t" << count_hypothesis << "\t" << count_trees << "\t" << count_set << "\t"
              << std::chrono::duration<double, std::milli>(t_end-t_start).count() << "\n";

    return clear_schema();
}

std::pair<int, int> treesolver::find_first_availble_cell() const {
    for (int i=0; i<N; ++i) {
        for (int j=0; j<N; ++j) {
            if (schema[i][j].content() == content_t::unknown) {
                return {i,j};
            }
        }
    }
    return {-1,-1};
}

schema_t treesolver::brute_solve() {
    auto t_start = std::chrono::high_resolution_clock::now();
    int nsteps = 0;

    while (!is_valid()) {
        auto p = find_first_availble_cell();
        //if (!m_silent) std::cout << "Selected cell (" << p.first << ", " << p.second << ")\n";
        create_tree_hypothesis(p.first, p.second);
        //if (!m_silent) std::cout << "valid so far " << std::boolalpha << is_valid_so_far() << std::endl;
        if (!is_valid_so_far()) {
            cancel_hypothesis();
            //if (!m_silent) std::cout << "(CANCELED) valid so far " << std::boolalpha << is_valid_so_far() << std::endl;
            while (not is_valid_so_far()) {
                cancel_hypothesis();
            }
            if (m_verbose) { std::cout << "(CANCEL) ";};
        }
        //if (!m_silent) std::cout << std::boolalpha << "Valid "  << is_valid() << std::endl;
        if (m_verbose) { std::cout << "End of itaration: hyp. level: " << current_hypothesis_level << "\n"; print_schema(); }
        count_hypothesis++;
    }

    auto t_end = std::chrono::high_resolution_clock::now();

    if (!m_silent) print_schema();
    std::cout << "brute=true"
              << " obvious=" << std::boolalpha << m_obvious
              << " smart=" << std::boolalpha << m_super
              << " (h t s)   \t" << count_hypothesis << "\t" << count_trees << "\t" << count_set << "\t"
              << std::chrono::duration<double, std::milli>(t_end-t_start).count() << "\n";

    return clear_schema();
}
