#include <iostream>
#include <deque>
#include <algorithm>
#include <random>
#include <cassert>
#include <iomanip>
#include <thread>
#include <boost/program_options.hpp>

template <typename T>
void show_table(T const& table, std::string s = "Table") {
    std::cout << "                  " << s << ": ";
    std::for_each(table.begin(), table.end(), [](int x) { std::cout << x << ", "; });
    std::cout << "<<<<\n";
}

std::pair<long long int, int>  game(bool silent, int mode, bool reshuffle, bool check_infinity, long long int count_limit, int N, int p) {

    if (!silent) std::cout << "Rubamazzetto\n";

    std::random_device rd;
    std::random_device rd1;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> deck(0, 1);
    std::uniform_int_distribution<> posr(0, N/2-1);

    std::deque<int> player[2] = {std::deque<int>(N/2,-1), std::deque<int>(N/2,-1)};

    switch (mode) {
    case 1:
        N=24;
        /* Period 53
          Table: -1, 3, 1, -1, -1, 2, 3, 2, 1, -1, -1, 1, <<<<
          Table: -1, -1, 3, -1, 1, -1, -1, 2, -1, 3, -1, 2, <<<<
        */
        player[0] = {-1, 3, 1, -1, -1, 2, 3, 2, 1, -1, -1, 1};
        player[1] = {-1, -1, 3, -1, 1, -1, -1, 2, -1, 3, -1, 2};
        break;
        /* Others
           Table: -1, 3, 1, 3, -1, 2, 3, 2, 1, -1, -1, 1, <<<<
           Table: -1, -1, 2, -1, -1, 1, -1, -1, 3, -1, 2, -1, <<<<
           After 11908510 games, something may be infinite
           Table: -1, -1, 2, -1, 3, -1, 2, -1, 1, -1, -1, 3, <<<<
           Table: 2, 2, 3, 1, -1, -1, 3, 1, -1, -1, 1, -1, <<<<
           After 37487960 games, something may be infinite
        */
    case 2: // Period 53
        N=24;

        player[0] = {-1, 3, 1, 3, -1, 2, 3, 2, 1, -1, -1, 1};
        player[1] = {-1, -1, 2, -1, -1, 1, -1, -1, 3, -1, 2, -1};
        break;
    case 3: // Period 53
        N=24;

        player[0] = {-1, -1, 2, -1, 3, -1, 2, -1, 1, -1, -1, 3};
        player[1] = {2, 2, 3, 1, -1, -1, 3, 1, -1, -1, 1, -1};
        break;
    case 4: // Period 53
        N=24;

        player[0] = {-1, -1, 3, -1, 2, -1, 2, -1, -1, 1, -1, -1};
        player[1] = {3, 3, 3, 1, 2, -1, 2, 1, -1, -1, 1, -1};
        break;
    case 5: // Period 26!!!!
        N=24;

        player[0] = {-1, -1, 2, -1, 2, -1, 3, -1, -1, 1, -1, -1};
        player[1] = {2, 3, 2, 1, 3, -1, 3, 1, -1, -1, 1, -1};
        break;
    case 6: // Period 53
        N=24;

        player[0] = {-1, 2, 1, -1, -1, 3, 2, 3, 1, -1, -1, 1};
        player[1] = {-1, -1, 2, -1, 1, -1, -1, 3, -1, 2, -1, 3};
        break;
    case 7: // From wikipedia
        N=40;
        player[0] = {-1, -1, 3, -1, 2, -1, 2, -1, 3, -1, 3, 1, -1, -1, -1, -1, -1, 3, 1, -1};
        player[1] = {-1, -1, 1, -1, -1, 2, -1, -1, -1, -1, 2, -1, -1, -1, -1, -1, -1, -1, 1, -1};
        break;
    case 0: {
        if (!silent) std::cout << "Shuffling and distributing decks\ny";
        int p[2] = {0,0};
        int plr;
        for (int i=0; i<4; ++i) {
            for (int j=1; j<=3; ++j) {
                int pos = posr(rd1);
                if (p[0] == N/2) {
                    plr = 1;
                    p[1]++;
                } else {
                    if (p[1] == N/2) {
                        plr = 0;
                        p[0]++;
                    } else {
                        plr = 1-deck(rd);
                        p[plr]++;

                    }
                }
                //std::cout << "pos = " << pos << ", deck " << plr << ", i " << i << ", " << j << "\n";
                if (player[plr][pos] == -1) {
                    player[plr][pos] = j;
                } else {
                    while (player[plr][pos] != -1) {
                        pos = (pos+1)%(N/2);
                        //std::cout << "." << pos;
                    }
                    player[plr][pos] = j;
                }
            }
        }
        break;
    }
    default:
        throw(std::runtime_error("Mode not supported"));
    }

    if (!silent) std::cout << "Player 1: ";
    std::for_each(player[0].begin(), player[0].end(), [silent](int x) { if (!silent) std::cout << x << ", "; });
    if (!silent) std::cout << "\n";
    if (!silent) std::cout << "Player 2: ";
    std::for_each(player[1].begin(), player[1].end(), [silent](int x) { if (!silent) std::cout << x << ", "; });
    if (!silent) std::cout << "\n";

    std::vector<int> player0(N/2);
    std::vector<int> player1(N/2);

    std::vector<int> snapshot0;
    std::vector<int> snapshot1;

    std::copy(player[0].begin(), player[0].end(), player0.begin());
    std::copy(player[1].begin(), player[1].end(), player1.begin());

#ifndef NDEBUG
    int count1_a = std::count_if(player[0].begin(), player[0].end(), [](int x) { return x==1; });

    int count2_a = std::count_if(player[0].begin(), player[0].end(), [](int x) { return x==2; });

    int count3_a = std::count_if(player[0].begin(), player[0].end(), [](int x) { return x==3; });

    int count_rest_a = std::count_if(player[0].begin(), player[0].end(), [](int x) { return x==-1; });

    int count1_b = std::count_if(player[1].begin(), player[1].end(), [](int x) { return x==1; });

    int count2_b = std::count_if(player[1].begin(), player[1].end(), [](int x) { return x==2; });

    int count3_b = std::count_if(player[1].begin(), player[1].end(), [](int x) { return x==3; });

    int count_rest_b = std::count_if(player[1].begin(), player[1].end(), [](int x) { return x==-1; });

    assert(count1_a+count1_b == 4);

    assert(count2_a+count2_b == 4);

    assert(count3_a+count3_b == 4);

    assert(count_rest_a+count_rest_b == N-12);
#endif

    bool infinity = false;
    std::deque<int> table;
    int drawing=0, waiting=1;
    bool finish = false;
    long long int count = 0;
    if (!silent) std::cout << count << ": Drawing: " << drawing << " [" << player[drawing].size() << "]"
              << ", waiting: " << waiting << " [" << player[waiting].size() << "]" << "\n";

    if (p == 0) {
        if (!silent) std::cout << "Taking snapshot at count == " << count << "\n";
        if (!silent) show_table(player[0]);
        if (!silent) show_table(player[1]);
        std::copy(player[0].begin(), player[0].end(), std::back_inserter(snapshot0));
        std::copy(player[1].begin(), player[1].end(), std::back_inserter(snapshot1));
    }

    while (not (player[0].empty() or player[1].empty())) {
        if (!silent) show_table(player[drawing], "drawing");
        if (!silent) show_table(player[waiting], "waiting");
        if (!silent) show_table(table);

        if (not table.empty() and table.front() != -1) {
            int value = table.front();
            for (int i=0; i<value; ++i) {
                table.push_front(player[drawing].front());
                player[drawing].pop_front();
                if (!silent) show_table(table);
                if (player[drawing].empty()) {
                    finish=true;
                    break;
                }
                if (table.front() != -1) break;
            }

            if (table.front() == -1) { //waiting wins

                while (not table.empty()) {
                    player[waiting].push_back(table.back());
                    table.pop_back();
                }
                if (reshuffle) {
                    std::random_device rrd;
                    std::mt19937 rg(rrd());

                    std::shuffle(player[waiting].begin(), player[waiting].end(), rg);
                    std::shuffle(player[drawing].begin(), player[drawing].end(), rg);
                }
                if (check_infinity and count > p) {
                    if (player[0].size() == snapshot0.size() and player[1].size() == snapshot1.size()) {
                        if (std::equal(player[0].begin(), player[0].end(), snapshot0.begin())) {
                            if (std::equal(player[1].begin(), player[1].end(), snapshot1.begin())) {
                                infinity=true;
                                std::cout << "Infinity found: periodicity " << count-p << "\n";
                            }
                        }
                    }
                }

                count++;
                if (!silent) std::cout << "All cards to player " << waiting << "\n";
                assert(table.size() == 0);
                if (count == p) {
                    if (!silent) std::cout << "Taking snapshot at count == " << count << "\n";
                    if (!silent) show_table(player[0], "Player 0");
                    if (!silent) show_table(player[1], "Player 1");
                    std::copy(player[0].begin(), player[0].end(), std::back_inserter(snapshot0));
                    std::copy(player[1].begin(), player[1].end(), std::back_inserter(snapshot1));
                }

            }
        } else {
            table.push_front(player[drawing].front());
            if (!silent) show_table(table);
            player[drawing].pop_front();
            if (player[drawing].empty()) {
                finish=true;
                break;
            }
        }
        if (!silent) std::cout << "\nswapping\n";
        std::swap(drawing, waiting);
        if (!silent) std::cout << count << ": Drawing: " << drawing << " [" << player[drawing].size() << "]"
                  << ", waiting: " << waiting << " [" << player[waiting].size() << "]" << "\n";
        assert(player[drawing].size()+player[waiting].size()+table.size() == N);

        if ((check_infinity and infinity) or (!check_infinity and count > count_limit)) {
            show_table(player0, "Player 0");
            show_table(player1, "Player 1");
            std::cout << "Current state\n";
            show_table(player[0], "player[0]");
            show_table(player[1], "player[1]");
            if (infinity)
                return {count, -2};
            else
                return {count, -1};
        }
    }

    assert(player[0].empty() or player[1].empty());
    assert(not (player[0].empty() and player[1].empty()));
    return {count, player[0].empty()?0:1};
}


template<typename T>
struct padded {

    T value;
    int pad[128/sizeof(int)-sizeof(T)];

    padded(T v) : value{v} {}
    padded() : value{} {}

    padded(padded const& v) : value{v.value} {}
    padded(padded&& v) : value{v.value} {}

    padded<T>& operator=(padded<T> const& v) { value=v.value; return *this;}
    padded<T>& operator=(padded<T>&& v) { value=v.value; return *this;}
    operator T&() {return value;};

    T& operator =(T const& v) {value=v; return value;}
};

int main(int argc, char** argv) {

    int N; // cards
    int n;
    int nt;
    int p; // dealy before check the periodicity
    bool silent = true;
    bool reshuffle = false;
    bool check_infinity = false;
    int mode; // 0 is random, 1, 2, .... are the manually plugged shuffles
    long long int count_limit;

    boost::program_options::options_description desc("Usage");
    desc.add_options()
        ("Cards,C", boost::program_options::value<int>(&N)->default_value(40), "Number of games to play\n")
        ("num,n", boost::program_options::value<int>(&n)->default_value(1), "Number of games to play\n")
        ("period,p", boost::program_options::value<int>(&p)->default_value(500), "Number of games to play\n")
        ("threads,t", boost::program_options::value<int>(&nt)->default_value(1), "Number of threads to use\n")
        ("mode,m", boost::program_options::value<int>(&mode)->default_value(0), "Number of threads to use\n")
        ("limit,l", boost::program_options::value<long long int>(&count_limit)->default_value(1000), "Number of threads to use\n")
        ("verbose,v", "Verbose mode\n")
        ("infinity,i", "Check recurrence for infinity\n")
        ("reshuffle,r", "Reshuffle decks after each take\n")
        ("help,?", "Produce help\n");

    boost::program_options::variables_map vm;
    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);

    if (vm.count("help")) {
        std::cout << "Do whatever you want\n";
        std::cout << desc << "\n";
        std::terminate();
    }

    if (vm.count("verbose")) {
        silent=false;
    }

    if (vm.count("infinity")) {
        check_infinity=true;
    }

    if (vm.count("reshuffle")) {
        reshuffle=true;
    }

    std::vector<padded<double>> avg(nt);
    std::vector<padded<int>> max(nt);
    std::vector<padded<int>> min(nt, std::numeric_limits<int>::max());
    std::vector<padded<int>> count_zero(nt);
    std::vector<padded<int>> long_game(nt);
    std::vector<padded<int>> win0(nt);
    std::vector<padded<int>> win1(nt);

    std::cout.precision(2);

    std::vector<std::thread> ths(nt);
    for (int t=0; t<nt; ++t) {
        ths[t] = std::thread([silent, mode, reshuffle, check_infinity, count_limit, N, n, nt, p, &avg, &min, &max, &count_zero, &long_game, &win0, &win1](int t) {
                for (int i=0; i<n/nt; ++i) {
                    const auto [moves, win] = game(silent, mode, reshuffle, check_infinity, count_limit, N, p);
                    if (win==-1) {
                        std::cout << "After " << i << " games, something may be infinite\n";
                    }

                    if (win==-2) {
                        std::cout << "After " << i << " games, something is DEFINITELY infinite!!\n";
                    }

                    avg[t] += double(moves);
                    if (max[t] < moves) max[t] = moves;
                    if (min[t] > moves) min[t] = moves;
                    if (moves==0) count_zero[t]++;
                    if (win==0) win0[t]++;
                    if (win==1) win1[t]++;
                    if (moves > 450) long_game[t]++;
                    if (t==0 and i%20000 == 0) {
                        std::cout << std::setw(5) << double(i)/double(n/nt)*100. << "%    \r";
                        std::cout.flush();
                    }
                }
            }, t
            );
    }


    std::for_each(ths.begin(), ths.end(), [](auto& th) { th.join(); });

    for (int i=1; i<nt; ++i) {
        avg[0] += avg[i];
        count_zero[0] += count_zero[i];
        long_game[0] += long_game[i];
        win0[0] += win0[i];
        win1[0] += win1[i];
        if (min[0] > min[i]) min[0]=min[i];
        if (max[0] < max[i]) max[0]=max[i];
    }

    avg[0] = avg[0]/n;
    std::cout.precision(10);

    std::cout << "Avg: " << avg[0] << ", "
              << "Min: " << min[0] << ", "
              << "Max: " << max[0] << ", "
              << "#>450: " << long_game[0] << ", "
              << "#0s: " << count_zero[0] << ", "
              << "zero wins: " << win0[0]/double(n) << ", "
              << "one wins: " << win1[0]/double(n)
              << "\n";
}
