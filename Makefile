treesolver.o: src/treesolver.hpp src/treesolver.cpp
	g++-8 -O3 -std=c++14 src/treesolver.cpp -c

schemas.o: src/schemas.cpp src/treesolver.hpp
	 g++-8 -O3 -std=c++14 $< -c -o $@ -I${BOOST_ROOT}/include

schemas: schemas.o treesolver.o
	 g++-8 -O3 -std=c++14 $< -o $@ treesolver.o -DBRUTE -L${BOOST_ROOT}/lib -lboost_program_options

rubamazzetto: src/rubamazzetto.cpp
	 g++-8 -g -std=c++17 $< -o $@

all: schemas

clean:
	rm *.o